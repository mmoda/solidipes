import streamlit as st
from IPython.display import display

from .. import loaders
from ..utils import solidipes_logging as logging
from ..utils import viewer_backends
from .viewer import Viewer

print = logging.invalidPrint
logger = logging.getLogger()


class Binary(Viewer):
    """Viewer for (unknown) binary"""

    def __init__(self, data=None):
        self.compatible_data_types = [loaders.Binary, str]
        self.data = []
        super().__init__(data)

    def add(self, data_container):
        """Append text to the viewer"""
        self.check_data_compatibility(data_container)

        if isinstance(data_container, loaders.DataContainer):
            self.data.append(data_container.file_info)
        else:
            raise RuntimeError("can only handle binary types")

    def show(self):
        if viewer_backends.current_backend == "jupyter notebook":
            for d in self.data:
                for k, v in d.data.items():
                    display(k, v)

        elif viewer_backends.current_backend == "streamlit":
            with st.container():
                logging.info(self.data)
                for d in self.data:
                    for k, v in d.data.items():
                        st.markdown(f"- {k} : {v}")
        else:  # python
            for d in self.data.items():
                for k, v in d.data:
                    print(k, k)
