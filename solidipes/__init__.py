"""Computational solid mechanics package for loading and visualizing files"""

from .utils import load_file

__all__ = ["load_file"]

__version__ = "0.0.0"
