.PHONY: doc

install:
	poetry install --with dev
	poetry run pre-commit install

lint:
	poetry run pre-commit run --all-files

build:
	poetry run python3 -m build

test:
	poetry run pytest

coverage:
	poetry run coverage run -m pytest
	poetry run coverage report -m

doc:
	$(MAKE) -C docs clean html

changelog:
	git checkout main
	poetry run git-changelog -o CHANGELOG.md
