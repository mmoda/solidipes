#?/bin/bash
# Execute from repository root

# Create requirements.txt from pyproject.toml using Poetry
poetry export \
	-f requirements.txt \
	--output requirements_test.txt \
	--with test \
	--without-hashes
