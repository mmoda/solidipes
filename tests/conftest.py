import os
import sys

import pytest

from solidipes.scripts.init import main as init

sys.path.append(os.path.join(os.path.dirname(__file__), "helpers"))


@pytest.fixture
def study_dir(tmp_path):
    """Setup a temporary directory with solidipes initialized."""

    class Args:
        directory = str(tmp_path)
        force = None

    args = Args()
    init(args)
    os.chdir(tmp_path)

    return tmp_path


@pytest.fixture
def user_path(tmp_path, monkeypatch):
    """Mock os.path.expanduser"""

    home = tmp_path / "HOME"
    home.mkdir()

    monkeypatch.setattr("os.path.expanduser", lambda path: path.replace("~", str(home)))
