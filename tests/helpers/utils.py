import os

# Path from /tests directory
assets_path_list = ["assets"]
tmp_path_list = ["tmp"]


def get_asset_path(filename):
    """Return the absolute path to an asset file"""
    return get_absolute_path(assets_path_list + [filename])


def get_tmp_path(filename):
    """Return the absolute path to a temporary file"""
    return get_absolute_path(tmp_path_list + [filename])


def get_absolute_path(path_list):
    """Return the absolute path to a file"""
    tests_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(tests_path, "..", *path_list)
    return file_path
