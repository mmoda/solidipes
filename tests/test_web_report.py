import os
import signal
import subprocess
import time

import pytest

streamlit_port = 8501
max_connection_tries = 20
delay_connection_trial = 1
max_loading_checks = 20
delay_page_load = 1


# Overriding the "sb" fixture to override the driver.
# from https://seleniumbase.io/help_docs/syntax_formats/#sb_sf_10
@pytest.fixture()
def sb(request):
    from selenium import webdriver
    from seleniumbase import BaseCase

    class BaseClass(BaseCase):
        def get_new_driver(self, *args, **kwargs):
            """This method overrides get_new_driver() from BaseCase."""
            options = webdriver.FirefoxOptions()
            options.add_argument("--headless")
            return webdriver.Firefox(options=options)

        def setUp(self):
            super().setUp()

        def base_method(self):
            pass

        def tearDown(self):
            self.save_teardown_screenshot()  # On failure or "--screenshot"
            super().tearDown()

    sb = BaseClass("base_method")
    sb.setUpClass()
    sb.setUp()
    yield sb
    sb.tearDown()
    sb.tearDownClass()


# Write class and fixture to terminate streamlit in case of failure
class WebReportLauncher:
    def __init__(self):
        self.streamlit_subprocess = None
        self.out = None
        self.err = None

    def launch(self, dir_path):
        self.streamlit_subprocess = subprocess.Popen(
            [
                "solidipes",
                "report",
                "web_report",
                dir_path,
                "--server.port",
                str(streamlit_port),
                "--server.headless",
                "true",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            preexec_fn=os.setsid,
        )
        return self

    def terminate(self):
        if not self.streamlit_subprocess:
            return

        os.killpg(os.getpgid(self.streamlit_subprocess.pid), signal.SIGTERM)
        self.out, self.err = self.streamlit_subprocess.communicate()
        self.streamlit_subprocess = None

    def get_outputs(self):
        return self.out, self.err


@pytest.fixture()
def web_report():
    web_report_launcher = WebReportLauncher()
    yield web_report_launcher
    web_report_launcher.terminate()


def init_study_dir(tmp_path, solidipes=True, git=False, git_remote=False):
    dir_path = tmp_path / "study"
    dir_path.mkdir()

    # Init solidipes
    if solidipes:
        subprocess.run(["solidipes", "init"], cwd=dir_path)

    # Init git and set remote
    if git or git_remote:
        subprocess.run(["git", "init", "-q"], cwd=dir_path)

    if git_remote:
        subprocess.run(
            ["git", "remote", "add", "origin", "https://domain.com/subdomain/user_name/project_name.git"], cwd=dir_path
        )

    return dir_path


def check_streamlit_is_loading(sb):
    return sb.is_element_present('div[data-testid="stStatusWidget"]')


def check_streamlit_errors(sb, dir_path, web_report):
    web_report.launch(dir_path)

    # Wait for streamlit to launch and open page
    for i in range(max_connection_tries):
        try:
            sb.open(f"http://localhost:{streamlit_port}")
            break
        except Exception:
            time.sleep(delay_connection_trial)

    # Let components load
    for i in range(max_loading_checks):
        time.sleep(delay_page_load)
        if not check_streamlit_is_loading(sb):
            break
    # Need additional delay for out and err to propagate
    time.sleep(delay_page_load)

    # Close streamlit and get outputs
    web_report.terminate()
    _, streamlit_errors = web_report.get_outputs()
    streamlit_errors = str(streamlit_errors).replace("\\n", "\n")
    error_found = "Traceback" in streamlit_errors or "already in use" in streamlit_errors

    if error_found:
        raise RuntimeError("Error encountered in Streamlit: \n{}".format(streamlit_errors))


def test_web_report(sb, tmp_path, web_report):
    dir_path = init_study_dir(tmp_path)
    check_streamlit_errors(sb, dir_path, web_report)


def test_web_report_without_solidipes(sb, tmp_path, web_report):
    dir_path = init_study_dir(tmp_path, solidipes=False)
    with pytest.raises(RuntimeError):
        check_streamlit_errors(sb, dir_path, web_report)


def test_web_report_with_git(sb, tmp_path, web_report):
    dir_path = init_study_dir(tmp_path, git=True)
    check_streamlit_errors(sb, dir_path, web_report)


def test_web_report_with_git_remote(sb, tmp_path, web_report):
    dir_path = init_study_dir(tmp_path, git_remote=True)
    check_streamlit_errors(sb, dir_path, web_report)
