import utils

import solidipes as sp


def test_load_binary():
    file_path = utils.get_asset_path("binary.bin")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Binary)
    assert file.file_info.size == 100


def test_load_without_extension():
    file_path = utils.get_asset_path("image")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Image)
    assert file.file_info.type == "image/jpeg"
    assert file._valid_loading() is False


def test_load_text():
    file_path = utils.get_asset_path("text.txt")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Text)
    assert file.text == "Hello World!\n"


def test_load_table():
    file_path = utils.get_asset_path("table.csv")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Table)

    file_path = utils.get_asset_path("table.xlsx")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Table)


def test_load_image_with_exif():
    file_path = utils.get_asset_path("image_with_exif.jpg")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Image)
    assert file.file_info.type == "image/jpeg"
    assert "ExifVersion" in file.exif_data._data_collection
