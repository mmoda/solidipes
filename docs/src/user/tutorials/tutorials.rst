.. _tutorial:
Tutorials
=========
Here, you can find step-by-step instructions on how to use the web interface to curate your data, describe your data (incl. metadata, abstract), and publish data to Zenodo.

For a quicker introduction, see the :ref:`Getting Started<getting-started>` section.


Select the **case** for which you want to use the Solidipes package :

.. hint::

   1. You would like to :ref:`download a dataset<zenodo-download>` from `Zenodo <https://zenodo.org/>`_.

   2. You want to :ref:`curate / describe / publish a dataset<starting-the-webreport>`


.. toctree::
   :maxdepth: 2
   :caption: Tutorials Contents

   setting_up/conda_venv
   download_from_zenodo/download_from_zenodo
   web_report/web_report
   web_report/metadata_description
   data_curation/data_curation
   sharing_with_renku/sharing_with_renku
   publishing_data/publishing_data
   troubleshooting/troubleshooting
   faq/faq
