.. _user-guide:
User Guide
==========

To quickly get started with Solidipes, take a look at the :ref:`getting-started` section.

For an applied tutorial on how to use Solidipes, follow the :ref:`tutorial` section. Specifically, you can:

- :ref:`Download a dataset<zenodo-download>` from any published Zenodo dataset.

- :ref:`Launch a web interface<starting-the-webreport>` to visualize and curate your data.

- :ref:`Publish<publishing-onto-zenodo>` your data on Zenodo with an attributed unique DOI.

For a more in depth description of the Solidipes capabilities, see the :ref:`reference` section.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   getting_started/getting_started.rst
   tutorials/tutorials.rst
   reference/reference.rst
