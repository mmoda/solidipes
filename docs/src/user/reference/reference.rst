.. _reference:
Reference
=========

Here you can find a more in depth explanation of the functionalities of solidipes.

Although the solidipes package was mainly constructed to be used via the web report interface (see :ref:`starting-the-webreport`) it can also be used via the command line interface :ref:`click here for the command line overview<command-line-overview>`.


.. toctree::
   :maxdepth: 2

   commands
   ref_initialization/ref_initialization
   ref_cloud/cloud_mount
   ref_curation/ref_curation
   ref_publishing_data/ref_publishing_data
