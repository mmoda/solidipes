.. _command-line-overview:
Command line overview for ``solidipes``
=======================================

``solidipes`` is a command line tool that allows you to initialize a study directory, upload it to Zenodo, and generate reports.

.. list-table:: command line options for ``solidipes``
   :header-rows: 1

   * - argument command
     - Attributes
   * -
      ``init`` :

      Initialize a study directory
     -
      positional arguments:
         - ``directory``: Path to the directory to initialize. Defaults to the current directory. Cannot be your home directory.
      optional arguments:
         - ``-h``: help message

         - ``--force``: Force the creation of solidipes configuration files in the current directory. WARNING: This could erase an existing .solidipes directory!
      usage: ``solidipes init [-h] [--force] [directory]``
   * -
      ``view``:

      Opens a new browser window with the data (e.g. table, image)
     -
      positional arguments:
         - ``path``: Path to file to be viewed

      optional arguments:
         - ``-h``: show this help message and exit
      usage: ``solidipes view [-h] path``
   * -
      ``upload``:

      Upload study to Zenodo
     -
      positional arguments:
         - ``directory``: Path to the directory containing the study to upload. Default directory: root of the current Solidipes study.
      optional arguments:
         - ``-h``: show this help message and exit

         - ``--sandbox``: use Zenodo sandbox test platform

         - ``--access_token ACCESS_TOKEN``: Provide the Zenodo token

         - ``--new-deposition``: create a new deposition instead of updating a previously created one

         - ``--existing-deposition [EXISTING_IDENTIFIER]``: URL or DOI of the unpublished study to update

      usage: ``solidipes upload [-h] [--sandbox] [--access_token ACCESS_TOKEN] [--new-deposition] [--existing-deposition [EXISTING_IDENTIFIER]] [directory]``
   * -
      ``report``:

      Generate a .py report
     -
      positional arguments:
         - ``{curation,jtcam,web_report}``: The report to generate
            - ``curation``: controls sanity of the data
            - ``jtcam``: opens a web report for the JTCAM Journal
            - ``web_report``: generates web_report (it also runs curation)
         - ``dir_path``: Path to directory containing data files

         - ``additional_arguments``: Arguments to forward to the report

      optional arguments:
         - ``-h``: show this help message and exit
      usage: ``solidipes report [-h] {curation,jtcam,web_report} dir_path ...``

   * -
      ``download``:

      Download content from Zenodo
     -
      positional arguments:
         - ``identifier``: URL or DOI of the study to download

         - ``destination``: Path to the destination folder

      optional arguments:
         - ``-h``: show this help message and exit

         - ``--only-metadata``: Only download metadata (overrides destination directory's metadata!)
      usage: ``solidipes download [-h] [--only-metadata] identifier [destination]``
