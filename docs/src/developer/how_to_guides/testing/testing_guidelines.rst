Testing
~~~~~~~
.. _testing-guidelines:
Testing Guidelines
-------------------

As a contributor you must add tests for your contributions if you plan to publicly contribute.

For this you might take the following into account:

1. Tests should be added to the ``tests`` folder.

2. IN CONSTRUCTION

You can then add tests.


Testing
-------

To run the tests
::
    python3 name_of_test_file.py

Adding Tests
-------------
Using ``assert`` you can test individual properties of your code.
