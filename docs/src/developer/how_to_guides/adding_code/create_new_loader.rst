Create new loader type (sound: wav, mp3, etc.)
----------------------------------------------

IN CONSTRUCTION

You can find in REFERENCEEEEEEEEEEE!!!!!! the list of all currently supported loader types.

This guide will follow you through the process of creating a new loader type (for example, a loader to read tables from a database).

If you want to add a new loader type, you need to:

1. create a newloader.py file in the loaders folder (``/loaders``):

In our example: ``/loaders/newloader.py``

2. create a new class in the ``newloader.py`` file, with the following structure::

    class NewLoader(File):
        """ New loader class to read from File """

        supported_mime_types = [ "list of supported mime types"]
        supported_extensions = [ "list of supported extensions"]

        def __init__(self, path):
            super().__init__(path)
            # your code here

        def _load_data(self, path):
            # your code here
            return self.data
        #additional functions if necessary

3. add the new loader type to the ``__init__.py`` file in the loaders folder (``/loaders/__init__.py``)
::
    from .newloader import NewLoader

and add the keyword "newloader" to the list of supported loaders::

    __all__ = ["File", .... , "NewLoader"]


3. add a test for the new loader type to the ``tests/test_common_files.py`` for all common loader types or add a separate test file in the ``tests`` folder.
