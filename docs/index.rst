.. Solidipes documentation master file, created by
   sphinx-quickstart on Wed Nov  9 22:59:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Solidipes' documentation!
====================================

*Armillaria solidipes* (or *armillaria ostoyae*) is the specie of the largest living organism on Earth, a fungus forming an underground network spanning 9.1 km\ :sup:`2`.
Solidipes is a Python package that aids the processes of **curating**, **publishing** and **sharing** research data, particularly tailored for the field of computational solid mechanics.

It is created within the scope of the `DCSM project <https://dcsm.readthedocs.io/>`_ (Dissemination of Computational Solid Mechanics), funded by `ETH Board <https://ethrat.ch/en/eth-domain/open-research-data/>`_ through an Open Research Data (ORD) grant.

If you want to use Solidipes, please refer to the :ref:`User Guide<user-guide>` section.

If you are interested in contributing to the development of Solidipes, please see the :ref:`Developer Guide<developer-guide>` section.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   src/user/index.rst
   src/developer/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
